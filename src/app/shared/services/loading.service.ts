import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class LoadingService {
  private loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  readonly isLoading$ = this.loading$.asObservable();

  setLoading(isLoading: boolean): void {
    this.loading$.next(isLoading);
  }
}
