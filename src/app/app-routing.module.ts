import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'employee-attendance',
    pathMatch: 'full',
  },
  {
    path: 'employee-attendance',
    loadChildren: () =>
      import('./employee-attendance/employee-attendance.module').then(
        (m) => m.EmployeeAttendanceModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
