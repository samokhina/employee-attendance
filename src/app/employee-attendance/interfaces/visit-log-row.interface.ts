import {IVisitLogCell} from './visit-log-cell.interface';

export interface IVisitLogRow {
  userName: string;
  visitLogCells: IVisitLogCell[];
}
