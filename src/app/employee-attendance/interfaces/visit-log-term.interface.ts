import {IVisitLogCell} from './visit-log-cell.interface';

export interface IVisitLogTerm {
  id: number;
  userId: number;
  startDate: Date;
  finishDate: Date;
  visitLogCells: Array<IVisitLogCell>;
}
