export enum VisitTypesEnum {
  PRESENCE = 'Присутствие',
  ABSENCE = 'Отсутствие',
  LATENESS = 'Опоздание',
  REMOTE_PRESENCE = 'Дистанционное присутствие',
  PROBATION = 'Стажировка',
  HOLIDAY = 'Выходной',
}
