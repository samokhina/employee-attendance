import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {FormControl} from '@angular/forms';

import {VisitTypesEnum} from '../../enums/visit-types.enum';

@Component({
  selector: 'app-selection-visit-type-dialog',
  templateUrl: './selection-visit-type-dialog.component.html',
  styleUrls: ['./selection-visit-type-dialog.component.scss'],
})
export class SelectionVisitTypeDialogComponent {
  selectedType: FormControl = new FormControl();
  visitTypes = VisitTypesEnum;

  constructor(
    public dialogRef: MatDialogRef<SelectionVisitTypeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public type: string
  ) {
    this.selectedType.setValue(type);
  }

  changeVisitType() {
    this.dialogRef.close(this.selectedType.value);
  }
}
