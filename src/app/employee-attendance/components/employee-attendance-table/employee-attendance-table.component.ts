import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {VisitTypesEnum} from '../../enums/visit-types.enum';
import {IVisitLogRow} from '../../interfaces/visit-log-row.interface';
import {IVisitLogCell} from '../../interfaces/visit-log-cell.interface';

import {SelectionVisitTypeDialogComponent} from '../selection-visit-type-dialog/selection-visit-type-dialog.component';

export interface IRowTable {
  [key: number]: IVisitLogCell | string | number;
}

const filterDays = (visitLogCells: IVisitLogCell[], type: VisitTypesEnum) =>
  visitLogCells.filter((cell) => cell.visitType === type).length;

@Component({
  selector: 'app-employee-attendance-table',
  templateUrl: './employee-attendance-table.component.html',
  styleUrls: ['./employee-attendance-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeAttendanceTableComponent implements OnInit, OnDestroy {
  @Input() set setVisitList(visitList: IVisitLogRow[]) {
    if (visitList && visitList.length) {
      console.log('visitlist', visitList);
      this.visitList = visitList;
      this.selectedCell = null;
      this.selectedCellIndex = null;
      this.updateDataTable();
    }
  }

  @Input() maxDays = 30;

  @Output()
  public onUpdateCell = new EventEmitter<{cell: IVisitLogCell; index: number}>();

  visitList: IVisitLogRow[] = [];
  days: string[] = [];

  columnsTable: string[] = ['userName', 'skippedDays', 'incompleteDays', 'warnings'];
  dataTable: IRowTable[] = [];

  visitTypes = VisitTypesEnum;

  selectedCellIndex: number;
  selectedCell: IVisitLogCell;

  private destroyed$: Subject<void> = new Subject<void>();

  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {
    for (let i = 1; i < this.maxDays + 1; i++) this.days.push(i.toString());
    this.columnsTable = [...this.columnsTable.slice(0, 1), ...this.days, ...this.columnsTable.slice(1, 4)];
  }

  ngOnDestroy(): void {
    this.destroyed$.complete();
    this.destroyed$.unsubscribe();
  }

  openDialog(cell: IVisitLogCell, index: number): void {
    if (cell.visitType !== this.visitTypes.HOLIDAY) {
      this.selectedCell = cell;
      this.selectedCellIndex = index;
      const dialogRef = this.dialog.open(SelectionVisitTypeDialogComponent, {
        data: cell.visitType,
      });
      this.dialogRefAfterClosed(dialogRef);
    }
  }

  private dialogRefAfterClosed(dialogRef: MatDialogRef<SelectionVisitTypeDialogComponent>): void {
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((type) =>
        this.onUpdateCell.next({
          cell: {
            ...this.selectedCell,
            visitType: type,
          },
          index: this.selectedCellIndex,
        })
      );
  }

  private updateDataTable(): void {
    this.dataTable = this.visitList.map((visitListItem) => {
      const cells: {[key: number]: IVisitLogCell} = {};

      visitListItem.visitLogCells.forEach((cell, index) => {
        Object.assign(cells, {
          [index]: visitListItem.visitLogCells[index],
        });
      });

      return Object.assign(
        {
          userName: visitListItem.userName,
          skippedDays: filterDays(visitListItem.visitLogCells, this.visitTypes.ABSENCE),
          incompleteDays: filterDays(visitListItem.visitLogCells, this.visitTypes.PROBATION),
          warnings: filterDays(visitListItem.visitLogCells, this.visitTypes.LATENESS),
        },
        cells
      );
    });
  }
}
