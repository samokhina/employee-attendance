import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Observable, of, Subject} from 'rxjs';
import {catchError, takeUntil, tap} from 'rxjs/operators';

import {MatSnackBar} from '@angular/material/snack-bar';

import {LoadingService} from '../shared/services/loading.service';
import {EmployeeAttendanceService} from './services/employee-attendance.service';

import {IVisitLogRow} from './interfaces/visit-log-row.interface';
import {IVisitLogCell} from './interfaces/visit-log-cell.interface';

@Component({
  selector: 'app-employee-attendance',
  templateUrl: './employee-attendance.component.html',
})
export class EmployeeAttendanceComponent implements OnInit, OnDestroy {
  visitList: IVisitLogRow[];
  maxDays = 30;

  private destroyed$: Subject<void> = new Subject<void>();

  constructor(
    private snackBar: MatSnackBar,
    private changeDetectorRef: ChangeDetectorRef,
    private loadingService: LoadingService,
    private employeeAttendanceService: EmployeeAttendanceService
  ) {
    this.loadingService.setLoading(true);
  }

  ngOnInit(): void {
    this.getVisitList()
      .pipe(
        takeUntil(this.destroyed$),
        tap((visitList) => (this.visitList = visitList))
      )
      .subscribe(() => this.loadingService.setLoading(false));
  }

  ngOnDestroy(): void {
    this.destroyed$.complete();
    this.destroyed$.unsubscribe();
  }

  updateCell(updateCell: {cell: IVisitLogCell; index: number}) {
    this.loadingService.setLoading(true);
    this.updateVisitItem(updateCell)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.loadingService.setLoading(false));
  }

  private getVisitList(): Observable<IVisitLogRow[]> {
    return this.employeeAttendanceService.getVisitList();
  }

  private updateVisitItem(updateCell: {cell: IVisitLogCell; index: number}): Observable<IVisitLogCell | null> {
    return this.employeeAttendanceService.updateVisitItem(updateCell.cell).pipe(
      tap(() => {
        const rowIndex = this.visitList.findIndex(
          (row) => row.visitLogCells.findIndex((cell) => cell.id === updateCell.cell.id) !== -1
        );

        this.visitList[rowIndex].visitLogCells[updateCell.index] = updateCell.cell;
        this.visitList = [...this.visitList];

        this.showSnackBarMessage('Ячейка была успешно изменена.');
      }),
      catchError(() => {
        this.showSnackBarMessage('Произошла ошибка. Попробуйте изменить еще раз.');
        return of(null);
      })
    );
  }

  private showSnackBarMessage(message: string): void {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }
}
