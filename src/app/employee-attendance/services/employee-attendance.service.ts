import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, delay, map} from 'rxjs/operators';

import {IVisitLogRow} from '../interfaces/visit-log-row.interface';
import {IVisitLogCell} from '../interfaces/visit-log-cell.interface';

@Injectable()
export class EmployeeAttendanceService {
  url = window.location.origin + `/assets/employee-attendance.json`;

  constructor(public http: HttpClient) {}

  /**
   * Получение табеля посещаемости сотрудников за период времени
   * @param period - период (начальная и конечная даты)
   */
  getVisitList(period?: {startDate: string; endDate: string}): Observable<IVisitLogRow[]> {
    return this.http.get<{rows: IVisitLogRow[]}>(this.url).pipe(
      delay(1000),
      map((response: {rows: IVisitLogRow[]}) => response?.rows ?? []),
      catchError(() => of([]))
    );
  }

  /**
   * Изменение ячейки
   * @param visitLogCell - ячейка
   */
  updateVisitItem(visitLogCell: IVisitLogCell): Observable<IVisitLogCell> {
    return of(visitLogCell).pipe(delay(1000));
    // return throwError('');
  }
}
